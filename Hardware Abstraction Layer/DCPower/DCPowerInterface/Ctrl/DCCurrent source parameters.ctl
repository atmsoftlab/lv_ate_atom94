RSRC
 LVCCLBVW  T  (      4   ,   DCPower.lvlibDCPower_Interface.lvclass   � �             < � @�      ����            ��V�#GE�.c�_ �          �pu���RI���~ �J��ُ ��	���B~       m7�`�VD���nkCK   ������ُ ��	���B~   ��� �9
L|��l%T          B LVCC    VILB    PTH0        DCPower_Interface.lvclass               &   &x�c�d`j`�� Č@������ �3p@e ��
�     G  x�c`��� H120� i4q0cS�2�]Pqf�Y!��@1�@H�F"Ǣ��8`�b6 �P&�     VIDS       �  �x�+`b`(�03a`f``fd`ch`H�OI�b` 	��	d�j�����i@��/��{4���l��T*������#�=�AB��
�/��?�XP��XC�5C	������/�c0���l\�� �:W �O��� �#�_��'�C���.4?`L��;�������3������������9�ߑDz�0a;؄�`�8`��zq�\�/�6���UU�Y]�Pe�	��b��I���U�;@A_���x���+Y����G���q� 1Ph3���a@<:�!�v1Bt�y섉ytz��4�y�G��.� %���+�ƚ.׾���.��� �U����8������b2P1�4�i ��ň0&���b>T��	��	d@�Z@�F����� ���P�����dO@r� T��j    �  16.0     �   16.0     �  16.0     �   16.0     �  16.0                       ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP       �  }x�͘]H[gǟ��ؓ�D�l��Ob�F��l'��z8lqڎ�76���NA�A��8��j�hi�v���n�[���h���� 5�.���u=9{�7'_FM�L.^���<���<'	@�'�.�T �\�`�+ ��!��,�p��d���0��pqҨ�ůx�)�q��>#׉IXǭ����U��+�wŨ ~�(����V8(\%q�����*OcA��h��8�i0Ȯ!�T@�&/oc)M*�R=#F[0#����$>r�̤R�<��˺ȚyXGQC������J��Y1z�i�L�un��H����w*ʕ�I��Rԅ�G�ڔN=�t�Xu��U�/F{y��O�ߓ�$2�k+�*�[OR��Z4��M�\���*+�90�ZM�D��sf�!e�a����l0Iat����q��>G���L`*�o�WRLG�QifF4�����W;KKKxZ����b4�����������U�kj�kr��Y��������R��=,^cq�F���gt|;_���t�������T]��B����ؕ���~�G*�9���L|QBMxk|���K�9�...f�З7Ӿ$ 6��X����^P��h?�_p��ld�!4ůTB/-uA��mha.�K�0Z��*�����f'g����\G�pίL�bԜ�p=ԀzRG֬��xd����.8Y�r��<:�LkQ��tw�Q�e�`MX�,�iee3a���`#�
GĞ�knq�-NWүq�E��o�V8i��j��݌a7xƊ�n�P�Zz7c��f,��*�%��-Fk�~��x=v�v�7�fn���HK�,�:�غ=cݖ����?g�D��9�Ɗ���M c�W%�M�g�[u����#r��ϧ���3�bgO��8�8�n
xh��׽?}>�8'?�:�}S����]!���餣�H'������I'� ݰ#�a-Mz3�~"�t��<��o"���ǁ�wg����+'������K�� ݸ3ү�Hמ#��ЕM���~j��Wg��|�o��ˊ�/ˊ����Ke���W��4�w���<��_��i�����a�xY1�[V����R���`|��7�����?X��?%����ϫP/����&,� �/��ℨ�
��5��Xd0�����v����|q�        R           BDHP        b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b           �      !_ni_LastKnownOwningLVClassCluster  ` �      0����     D   +  7����  �               4@p 	niDCPower      �              Resource  <@p 	niDCPower      �              Resource Initial  @0����Internal name @0����channel name  @ output function @
 Current Level @
 Voltage Limit @
 Current Level Range @
 Voltage Limit Range @
 
Resistance  p �       DCPower.lvlibDCPower_Interface.lvclassDCCurrent source parameters.ctl  @P     	 

DC Current  @
 Voltage Level @
 Voltage Range @
 Current Limit @
 Current Limit Range q �       DCPower.lvlibDCPower_Interface.lvclass DC Voltage source parameters.ctl  @P      

DC Voltage  @
 Pulse On Time (Sec) @
 Pulse Bias Delay (Sec)  @
 Pulse Off Time (Sec)  @
 Pulse Bias Current Limit  @
 Pulse Current Limit Range @
 Pulse Current Limit @
 Pulse Bias Voltage Level  @
 Pulse Voltage Level Range @
 Pulse Voltage Level @ Number of Pulses  � �       DCPower.lvlibDCPower_Interface.lvclass#Pulse Voltage source parameters.ctl ,@P 
          Pulce Voltage @
 Pulse Bias Voltage Limit  @
 Pulse Voltage Limit Range @
 Pulse Voltage Limit @
 Pulse Bias Current Level  @
 Pulse Current Level Range @
 Pulse Current Level � �       DCPower.lvlibDCPower_Interface.lvclass#Pulse Current source parameters.ctl ,@P 
          ! Pulce Current @!Enable Output @!	Autorange q �       DCPower.lvlibDCPower_Interface.lvclassChannel Configuration Data.ctl "@P       " # $Source  @@ ���� %Source  @!Debuging mode @!
Synch mode  @!Variable resistance .@P     & ' ( )DCPower_Interface.lvclass  *                 3   (         	                              /  �x�}O�N�@X�REAP���_��?Ф�Ą���S�diI������Жbb'�=gf��)�{|�`�e�8=��ؒzM�3+ѳx;�IN\za��������q�L
��7�Ou��<��lbw6�W��K�$�L(�Ck��q3�t���7&3k�-���B�s����Phx�@W O�Òy#4٨�����-�Ȯ���*]���4B�o*:�q��ˈ�0,�.�P�kIz��.�������<50����9�q���T�hV���*7����U��>�Z���#2#�Y��*.�E~ RRb    e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  T  (      4               4     LIBN      TLVSR      hRTSG      |CCST      �LIvi      �CONP      �TM80      �DFDS      �LIds      �VICD      vers     GCPR      �ICON      �icl8      �CPC2      �LIfp      �FPHb      �FPSE      �VPDP      LIbd       BDHb      4BDSE      HVITS      \DTHP      pMUID      �HIST      �VCTP      �FTAB      �    ����                           0        ����       �        ����       �        ����       �        ����      8        ����      @        ����      l        ����      �        ����      �       ����      �       ����      �       ����      �       	����      �       
����      �        ����      �        ����      �        ����      x        ����      |        ����      �        ����      �        ����      4        ����      <        ����      D        ����      T        ����      �        ����      �        ����      X        ����      `        ����      h        ����      �       �����      �    DCCurrent source parameters.ctl